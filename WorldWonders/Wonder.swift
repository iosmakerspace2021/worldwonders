//
//  Wonder.swift
//  WorldWonders
//
//  Created by Olzhas Akhmetov on 29.03.2021.
//
//{
//name: "Колизей",
//region: "Европа",
//location: "Рим, Италия",
//flag: "https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Flag_of_Italy.svg/44px-Flag_of_Italy.svg.png",
//picture: "https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Colosseum_in_Rome%2C_Italy_-_April_2007.jpg/200px-Colosseum_in_Rome%2C_Italy_-_April_2007.jpg"
//}

import UIKit
import SwiftyJSON

class Wonder {
    var name: String!
    var region: String!
    var location: String!
    var flag: String!
    var picture: String!
    
    init() {
        name = ""
        region = ""
        location = ""
        flag = ""
        picture = ""
    }
    
    init(json: JSON) {
        if let temp = json["name"].string {
            name = temp
        }
        if let temp = json["region"].string {
            region = temp
        }
        if let temp = json["location"].string {
            location = temp
        }
        if let temp = json["flag"].string {
            flag = temp
        }
        if let temp = json["picture"].string {
            picture = temp
        }
    }

}
